package capstone

class Role {
    int RoleID
    String RoleDesc

    static hasMany = [users: SiteUser]

    static constraints = {
    }
}
