package capstone

class SiteUser {
    int UserID
    int GradeLevel
    String FirstName
    String LastName
    String Email
    String PasswordHash
    static hasOne = [role: Role]
    static constraints = {

    }
}
