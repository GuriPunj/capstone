package capstone

class Question {
    int QuestionID
    String QuestionText
    String PictureURL
    Date CreationDate
    int GradeLevel
    int SubCategoryID
    int DifficultyStars
    Boolean IsTestQues
    Boolean IsPracticeQues
    static constraints = {
    }
}
