<!DOCTYPE html>
<html>
<head>
    <title><g:layoutTitle default="Math Competition App"/></title>
    <asset:stylesheet src="application.css"/>
    <asset:stylesheet src="custom.css"/>
    <g:layoutHead/>
</head>
<body>
<g:render template="/navbar" />
<g:layoutBody/>
<g:render template="/footer" />
<asset:javascript src="application.js"/>
</body>
</html>