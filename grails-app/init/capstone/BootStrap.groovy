package capstone
import capstone.SiteUser
import capstone.Role

class BootStrap {

    def init = { servletContext ->
        new Role(RoleID: 1,RoleDesc: "Student")
                .addToUsers(new SiteUser(UserID: 1,FirstName: "xyz",LastName: "xyz",GradeLevel: 1,Email: "a@aa.com",PasswordHash: "123")).save(flush:true, failOnError:true)
    }
    def destroy = {
    }
}
